var qds = require('..'),
    expect = require('chai').expect;

describe('QDataStream(write)', function() {
  describe('booleans', function() {
    it('should encode a bool', function(done) {
      var encoded = qds.writeBool(true).toString('base64');
      expect(encoded).to.equal('AQ==');
      done();
    });
  });

  describe('numbers', function() {
    it('should encode an integer', function(done) {
      var encoded = qds.writeNumber(42).toString('base64');
      expect(encoded).to.equal('AAAAKg==');
      done();
    });

    it('should encode a double', function(done) {
      var encoded = qds.writeNumber(12.34).toString('base64');
      expect(encoded).to.equal('QCiuFHrhR64=');
      done();
    });
  });

  describe('strings', function() {
    it('should encode an empty QString', function(done) {
      var encoded = qds.writeString('').toString('base64');
      expect(encoded).to.equal('/////w==');
      done();
    });

    it('should encode a QString', function(done) {
      var encoded = qds.writeString('test string').toString('base64');
      expect(encoded).to.equal('AAAAFgB0AGUAcwB0ACAAcwB0AHIAaQBuAGc=');
      done();
    });
  });

  describe('variants', function() {
    it('should encode QString in QVariant', function(done) {
      var encoded = qds.writeVariant('test string').toString('base64');
      expect(encoded).to.equal('AAAACgAAAAAWAHQAZQBzAHQAIABzAHQAcgBpAG4AZw==');
      done();
    });

    it('should encode integer in QVariant', function(done) {
      var encoded = qds.writeVariant(42).toString('base64');
      expect(encoded).to.equal('AAAAAwAAAAAq');
      done();
    });

    it('should encode double in QVariant', function(done) {
      var encoded = qds.writeVariant(12.34).toString('base64');
      expect(encoded).to.equal('AAAABgBAKK4UeuFHrg==');
      done();
    });

    it('should encode a boolean in QVariant', function(done) {
      var encoded = qds.writeVariant(true).toString('base64');
      expect(encoded).to.equal('AAAAAQAB');
      done();
    });
  });

  describe('variant lists', function() {
    it('should decode a QVariantList of a single QString', function(done) {
      var encoded = qds.writeVariantList(['test string']).toString('base64');
      expect(encoded).to.equal('AAAACQAAAAABAAAACgAAAAAWAHQAZQBzAHQAIABzAHQAcgBpAG4AZw==');
      done();
    });

    it('should decode a QVariantList of multiple QStrings', function(done) {
      var encoded = qds.writeVariantList(['string1', 'string2', 'string3']).toString('base64');
      expect(encoded).to.equal('AAAACQAAAAADAAAACgAAAAAOAHMAdAByAGkAbgBnADEAAAAKAAAAAA4AcwB0AHIAaQBuAGcAMgAAAAoAAAAADgBzAHQAcgBpAG4AZwAz');
      done();
    });
  });

  describe('variant maps', function() {
    it('should encode a QVariantMap of QString', function(done) {
      var encoded = qds.writeVariantMap({testString: 'test string'}).toString('base64');
      expect(encoded).to.equal('AAAACAAAAAABAAAAFAB0AGUAcwB0AFMAdAByAGkAbgBnAAAACgAAAAAWAHQAZQBzAHQAIABzAHQAcgBpAG4AZw==');
      done();
    });
  });

});
