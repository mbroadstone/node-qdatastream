"use strict";

function swapBytes(buffer) {
  var l = buffer.length;
  if (l & 0x01) {
    throw new Error('buffer length must be even');
  }

  for (var i = 0; i < l; i += 2) {
    var a = buffer[i];
    buffer[i] = buffer[i+1];
    buffer[i+1] = a;
  }

  return buffer;
}

function _readQInt8(context) {
  var result = context.buffer.readInt8(context.pos);
  context.pos += 1;
  return result;
}

function _readQUInt8(context) {
  var result = context.buffer.readUInt8(context.pos);
  context.pos += 1;
  return result;
}

function _readQInt16(context) {
  var result = context.buffer.readInt16BE(context.pos);
  context.pos += 2;
  return result;
}

function _readQUInt16(context) {
  var result = context.buffer.readUInt16BE(context.pos);
  context.pos += 2;
  return result;
}

function _readQInt32(context) {
  var result = context.buffer.readInt32BE(context.pos);
  context.pos += 4;
  return result;
}

function _readQUInt32(context) {
  var result = context.buffer.readUInt32BE(context.pos);
  context.pos += 4;
  return result;
}

function _readDouble(context) {
  var result = context.buffer.readDoubleBE(context.pos);
  context.pos += 8;
  return result;
}

function _readBool(context) {
  var result = context.buffer.readInt8(context.pos);
  context.pos += 1;
  return !!result;
}

function _readString(context) {
  var bytes = context.buffer.readUInt32BE(context.pos);
  context.pos += 4;

  if ((bytes & 0xFFFFFFFF) < 0)
    return '';

  var end = context.pos + bytes;
  var utf16le = new Buffer(bytes);
  context.buffer.copy(utf16le, 0, context.pos, end);
  var utf16be = swapBytes(utf16le);
  context.pos += bytes;

  return utf16be.toString('ucs2');
}

function _readVariant(context) {
  var type = context.buffer.readUInt32BE(context.pos);
  context.pos += 4;

  var nullFlag = context.buffer.readInt8(context.pos);
  context.pos += 1;

  switch (type) {
    case 0:
      return _readUndefined(context);
    case 1:
      return _readBool(context);
    case 2:
      return _readQInt32(context);
    case 3:
      return _readQUInt32(context);
    case 6:
      return _readDouble(context);
    case 8:
      return _readVariantMap(context);
    case 9:
      return _readVariantList(context);
    case 10:
      return _readString(context);
    case 11:
      return _readStringList(context);

    default:
      throw new Error('unhandled variant type: ' + type);
  }
}

function _readUndefined(context) {
  _readString(context);
  return null;
}

function _readStringList(context) {
  var size = context.buffer.readUInt32BE(context.pos);
  context.pos += 4;

  var result = [];
  for (var i = 0; i < size; i++) {
    var string = _readString(context);
    result.push(string);
  }

  return result;
}

function _readVariantList(context) {
  var size = context.buffer.readUInt32BE(context.pos);
  context.pos += 4;

  var result = [];
  for (var i = 0; i < size; i++) {
    var variant = _readVariant(context);
    result.push(variant);
  }

  return result;
}

function _readVariantMap(context) {
  var size = context.buffer.readUInt32BE(context.pos);
  context.pos += 4;

  var result = {};
  for (var i = 0; i < size; i++) {
    var key = _readString(context);
    var value = _readVariant(context);
    result[key] = value;
  }

  return result;
}

function _writeBool(bool) {
  var result = new Buffer(1);
  result.writeInt8(bool, 0);
  return result;
}

function _writeString(string) {
  var stringBuffer = swapBytes(new Buffer(string, 'ucs2'));
  if (stringBuffer.length === 0) {
    var emptyHeader = new Buffer(4);
    emptyHeader.writeUInt32BE('0xFFFFFFFF', 0);
    return emptyHeader;
  }

  var header = new Buffer(4);
  header.writeUInt32BE(stringBuffer.length, 0);
  return Buffer.concat([header, stringBuffer]);
}

function _isInteger(number) {
  return !(isNaN(number * 1) || (number % 1) !== 0 );
}

function _writeNumber(number) {
  var result;
  if (_isInteger(number)) {
    result = new Buffer(4);
    result.writeInt32BE(number, 0);
  } else {
    result = new Buffer(8);
    result.writeDoubleBE(number, 0);
  }

  return result;
}

function _writeVariant(value) {
  if (value instanceof Array) {
    var listHeader = new Buffer(9);
    listHeader.writeUInt32BE(9, 0);
    listHeader.writeInt8(0, 4);
    listHeader.writeUInt32BE(value.length, 5);
    var variants = value.map(_writeVariant);
    variants.unshift(listHeader);
    return Buffer.concat(variants);
  }

  if (value instanceof Object) {
    var mapHeader = new Buffer(9);
    mapHeader.writeUInt32BE(8, 0);
    mapHeader.writeInt8(0, 4);
    mapHeader.writeUInt32BE(Object.keys(value).length, 5);

    var buffers = [];
    for (var key in value) {
      if (value.hasOwnProperty(key))
        buffers.push(Buffer.concat([_writeString(key), _writeVariant(value[key])]));
    }

    buffers.unshift(mapHeader);
    return Buffer.concat(buffers);
  }

  var header = new Buffer(5); // type + nullFlag
  switch (typeof value) {
    case 'string':
      header.writeUInt32BE(10, 0);
      header.writeInt8(0, 4);
      return Buffer.concat([header, _writeString(value)]);

    case 'number':
      if (_isInteger(value)) {
        header.writeUInt32BE(3, 0);
        header.writeInt8(0, 4);
      } else {
        header.writeUInt32BE(6, 0);
        header.writeInt8(0, 4);
      }
      return Buffer.concat([header, _writeNumber(value)]);

    case 'boolean':
      header.writeUInt32BE(1, 0);
      header.writeInt8(0, 4);
      return Buffer.concat([header, _writeBool(value)]);

    default:
      throw new Error('unhandled variant type: ' + typeof value);
  }
}

// EXPORTS
module.exports = {
  readVariantList: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readVariant(context);
  },

  readVariantMap: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readVariant(context);
  },

  readVariant: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readVariant(context);
  },

  readString: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readString(context);
  },

  readBool: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readBool(context);
  },

  readQInt8: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readQInt8(context);
  },

  readQUInt8: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readQUInt8(context);
  },

  readQInt16: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readQInt16(context);
  },

  readQUInt16: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readQUInt16(context);
  },

  readQInt32: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readQInt32(context);
  },

  readQUInt32: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readQUInt32(context);
  },

  readDouble: function(buffer) {
    var context = {
      buffer: buffer,
      pos: 0
    };

    return _readDouble(context);
  }
};

module.exports.writeBool = _writeBool;
module.exports.writeString = _writeString;
module.exports.writeNumber = _writeNumber;
module.exports.writeVariant = _writeVariant;
module.exports.writeVariantList = _writeVariant;
module.exports.writeVariantMap = _writeVariant;
